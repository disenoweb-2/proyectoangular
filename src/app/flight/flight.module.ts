import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FlightListComponent } from './flight-list/flight-list.component';
import { FlightEditComponent } from './flight-edit/flight-edit.component';
import { FlightService } from './flight.service';
import { FLIGHT_ROUTES } from './flight.routes';

@NgModule({
  imports: [
    CommonModule,  // Módulo de Angular para características comunes
    FormsModule,    // Módulo para trabajar con formularios
    RouterModule.forChild(FLIGHT_ROUTES)  // Configuración de rutas específicas para vuelos
  ],
  declarations: [
    FlightListComponent,   // Componente para mostrar la lista de vuelos
    FlightEditComponent   // Componente para editar detalles de vuelos
  ],
  providers: [FlightService], // Proveedor del servicio relacionado con vuelos
  exports: []  // No se exporta nada en este módulo
})
export class FlightModule { }

