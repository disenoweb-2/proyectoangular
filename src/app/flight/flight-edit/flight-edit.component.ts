import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FlightService } from '../flight.service';
import { Flight } from '../flight';
import { map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-flight-edit',
  templateUrl: './flight-edit.component.html'
})
export class FlightEditComponent implements OnInit {
  // Declaración de propiedades de la clase
  id!: string;
  flight!: Flight;
  feedback: any = {};

  // Constructor del componente
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private flightService: FlightService
  ) {}

  // Método ngOnInit que se ejecuta al iniciar el componente
  ngOnInit() {
    this
      .route
      .params
      .pipe(
        map(p => p.id), // Mapea los parámetros de la URL para obtener el ID
        switchMap(id => {
          if (id === 'new') { return of(new Flight()); } // Si el ID es 'new', crea un nuevo objeto Flight
          return this.flightService.findById(id); // De lo contrario, busca el vuelo por su ID
        })
      )
      .subscribe(flight => {
          this.flight = flight; // Asigna el vuelo recuperado a la propiedad flight
          this.feedback = {}; // Limpia cualquier retroalimentación existente
        },
        err => {
          this.feedback = {type: 'warning', message: 'Error de carga'}; // En caso de error, muestra un mensaje de advertencia
        }
      );
  }

  // Método para guardar los cambios en el vuelo
  save() {
    this.flightService.save(this.flight).subscribe(
      flight => {
        this.flight = flight; // Actualiza la propiedad flight con el vuelo guardado
        this.feedback = {type: 'success', message: 'Se guardó correctamente!'}; // Muestra un mensaje de éxito
        setTimeout(() => {
          this.router.navigate(['/flights']); // Redirige a la lista de vuelos después de un segundo
        }, 1000);
      },
      err => {
        this.feedback = {type: 'warning', message: 'Error al guardar.'}; // En caso de error al guardar, muestra un mensaje de advertencia
      }
    );
  }

  // Método para cancelar y volver a la lista de vuelos
  cancel() {
    this.router.navigate(['/flights']);
  }
}



