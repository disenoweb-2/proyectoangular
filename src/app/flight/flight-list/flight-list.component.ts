import { Component, OnInit } from '@angular/core';
import { FlightFilter } from '../flight-filter';
import { FlightService } from '../flight.service';
import { Flight } from '../flight';

@Component({
  selector: 'app-flight',
  templateUrl: 'flight-list.component.html'
})
export class FlightListComponent implements OnInit {
  // Declaración de propiedades de la clase
  filter = new FlightFilter(); // Filtro para la búsqueda de vuelos
  selectedFlight!: Flight; // Vuelo seleccionado
  feedback: any = {}; // Retroalimentación de acciones

  // Getter para obtener la lista de vuelos del servicio
  get flightList(): Flight[] {
    return this.flightService.flightList;
  }

  // Constructor del componente
  constructor(private flightService: FlightService) {}

  // Método ngOnInit que se ejecuta al iniciar el componente
  ngOnInit() {
    this.search(); // Inicia la búsqueda de vuelos al cargar el componente
  }

  // Método para realizar una búsqueda de vuelos
  search(): void {
    this.flightService.load(this.filter); // Invoca el servicio para cargar la lista de vuelos con el filtro actual
  }

  // Método para seleccionar un vuelo
  select(selected: Flight): void {
    this.selectedFlight = selected; // Establece el vuelo seleccionado
  }

  // Método para eliminar un vuelo
  delete(flight: Flight): void {
    if (confirm('¿Estás seguro?')) {
      this.flightService.delete(flight).subscribe(
        () => {
          this.feedback = { type: 'success', message: 'Se eliminó correctamente!' }; // Muestra un mensaje de éxito
          setTimeout(() => {
            this.search(); // Vuelve a realizar una búsqueda después de un segundo
          }, 1000);
        },
        err => {
          this.feedback = { type: 'warning', message: 'Error al eliminar.' }; // Muestra un mensaje de advertencia en caso de error
        }
      );
    }
  }
}

