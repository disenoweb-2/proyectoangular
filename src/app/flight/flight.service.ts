import { Flight } from './flight'; // Importa el modelo Flight para representar datos de vuelos
import { FlightFilter } from './flight-filter'; // Importa el modelo FlightFilter para filtros de búsqueda
import { Injectable } from '@angular/core'; // Anotación @Injectable para inyectar el servicio en componentes
import { EMPTY, Observable } from 'rxjs'; // Importa Observable y EMPTY de RxJS para manejar flujos de datos
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'; // Importa HttpClient y otros para hacer solicitudes HTTP

@Injectable()
export class FlightService {
  flightList: Flight[] = []; // Lista de vuelos

  constructor(private http: HttpClient) {
  }

  // Método para buscar un vuelo por su ID
  findById(id: string): Observable<Flight> {
    const url = `http://www.angular.at/api/flight/${id}`;
    const params = { 'id': id };
    const headers = new HttpHeaders().set('Accept', 'application/json');
    return this.http.get<Flight>(url, { params, headers });
  }

  // Método para cargar una lista de vuelos con filtro
  load(filter: FlightFilter): void {
    this.find(filter).subscribe(result => {
        this.flightList = result;
      },
      err => {
        console.error('error loading', err);
      }
    );
  }

  // Método para buscar vuelos con filtro
  find(filter: FlightFilter): Observable<Flight[]> {
    const url = `http://www.angular.at/api/flight`;
    const headers = new HttpHeaders().set('Accept', 'application/json');

    const params = {
      'from': filter.from,
      'to': filter.to,
    };

    return this.http.get<Flight[]>(url, { params, headers });
  }

  // Método para guardar un vuelo (crear o actualizar)
  save(entity: Flight): Observable<Flight> {
    let params = new HttpParams();
    let url = '';
    const headers = new HttpHeaders().set('content-type', 'application/json');
    if (entity.id) {
      url = `http://www.angular.at/api/flight/${entity.id.toString()}`;
      params = new HttpParams().set('ID', entity.id.toString());
      return this.http.put<Flight>(url, entity, { headers, params });
    } else {
      url = `http://www.angular.at/api/flight`;
      return this.http.post<Flight>(url, entity, { headers, params });
    }
  }

  // Método para eliminar un vuelo
  delete(entity: Flight): Observable<Flight> {
    let params = new HttpParams();
    let url = '';
    const headers = new HttpHeaders().set('content-type', 'application/json');
    if (entity.id) {
      url = `http://www.angular.at/api/flight/${entity.id.toString()}`;
      params = new HttpParams().set('ID', entity.id.toString());
      return this.http.delete<Flight>(url, { headers, params });
    }
    return EMPTY;
  }
}


