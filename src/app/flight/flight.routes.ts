import { Routes } from '@angular/router';
import { FlightListComponent } from './flight-list/flight-list.component';
import { FlightEditComponent } from './flight-edit/flight-edit.component';

export const FLIGHT_ROUTES: Routes = [
  {
    path: 'flights',              // Ruta para mostrar la lista de vuelos
    component: FlightListComponent  // Componente a mostrar para la lista de vuelos
  },
  {
    path: 'flights/:id',          // Ruta para editar detalles de un vuelo por su ID
    component: FlightEditComponent  // Componente a mostrar para la edición de vuelos
  }
];
