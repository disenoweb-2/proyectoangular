export class Flight {
  id!: number;      // Propiedad para el ID del vuelo
  from!: string;    // Propiedad para el lugar de origen del vuelo
  to!: string;      // Propiedad para el lugar de destino del vuelo
  date!: Date;      // Propiedad para la fecha del vuelo
}

