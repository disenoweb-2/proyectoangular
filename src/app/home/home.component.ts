import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  constructor(
    private route: ActivatedRoute) {
  }

  needsLogin: boolean | undefined; // Indica si se necesita iniciar sesión
  _userName: string = ''; // Nombre de usuario

  ngOnInit() {
    this.needsLogin = !!this.route.snapshot.params['needsLogin']; // Obtiene el valor de 'needsLogin' desde los parámetros de la URL
  }

  get userName(): string {
    return this._userName; // Getter para obtener el nombre de usuario
  }

  login(): void {
    this._userName = 'Diego'; // Método para realizar el inicio de sesión y establecer el nombre de usuario
  }

  logout(): void {
    this._userName = ''; // Método para cerrar la sesión y eliminar el nombre de usuario
  }
}

document.addEventListener("DOMContentLoaded", function () {
  const loginForm = document.getElementById("loginForm") as HTMLFormElement;
  loginForm.addEventListener("submit", function (e) {
      e.preventDefault();

      const username = (document.getElementById("username") as HTMLInputElement).value;
      const password = (document.getElementById("password") as HTMLInputElement).value;

      // Validación del usuario y la contraseña
      if (username === "Diego" && password === "12345") {
          // Inicio de sesión exitoso
          alert("Inicio de sesión exitoso");
      } else {
          // Mostrar un mensaje de error.
          alert("Usuario o contraseña incorrectos");
      }
  });
});