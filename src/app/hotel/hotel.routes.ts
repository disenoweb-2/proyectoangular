import { Routes } from '@angular/router';
import { HotelListComponent } from './hotel-list/hotel-list.component';
import { HotelEditComponent } from './hotel-edit/hotel-edit.component';

// Define las rutas para la gestión de hoteles en la aplicación.
export const HOTEL_ROUTES: Routes = [
  {
    path: 'hotels',
    component: HotelListComponent
  },
  // Ruta para mostrar la lista de hoteles, carga el componente HotelListComponent.

  {
    path: 'hotels/:id',
    component: HotelEditComponent
  }
  // Ruta para editar un hotel específico, utiliza el componente HotelEditComponent.
];
