import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HotelService } from '../hotel.service';
import { Hotel } from '../hotel';
import { map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-hotel-edit',
  templateUrl: './hotel-edit.component.html'
})
export class HotelEditComponent implements OnInit {

  id!: string;            // Variable para almacenar el ID del hotel.
  hotel!: Hotel;          // Variable para almacenar los detalles del hotel.
  feedback: any = {};     // Variable para almacenar comentarios o retroalimentación.

  constructor(
    private route: ActivatedRoute,    // Servicio para acceder a los parámetros de la URL.
    private router: Router,           // Servicio para la navegación en la aplicación.
    private hotelService: HotelService // Servicio que proporciona operaciones relacionadas con hoteles.
  ) {
  }

  ngOnInit() {
    this
      .route
      .params
      .pipe(
        map(p => p['id']),   // Extrae el ID de los parámetros de la URL.
        switchMap(id => {
          if (id === 'new') { return of(new Hotel()); }
          // Si el ID es 'new', crea un nuevo objeto Hotel vacío.
          return this.hotelService.findById(id);
          // De lo contrario, busca los detalles del hotel por ID.
        })
      )
      .subscribe({
        next: hotel => {
          this.hotel = hotel;  // Asigna los detalles del hotel.
          this.feedback = {};  // Borra cualquier retroalimentación existente.
        },
        error: err => {
          this.feedback = {type: 'warning', message: 'Error al cargar'};
          // Captura errores y establece un mensaje de advertencia.
        }
      });
  }

  save() {
    this.hotelService.save(this.hotel).subscribe({
      next: hotel => {
        this.hotel = hotel;  // Actualiza los detalles del hotel.
        this.feedback = {type: 'success', message: 'Se guardó correctamente!'};
        // Establece un mensaje de éxito.
        setTimeout(async () => {
          await this.router.navigate(['/hotels']);
        }, 1000);
        // Después de 1 segundo, redirige a la lista de hoteles.
      },
      error: err => {
        this.feedback = {type: 'warning', message: 'Error al guardar'};
        // Captura errores y establece un mensaje de advertencia.
      }
    });
  }

  async cancel() {
    await this.router.navigate(['/hotels']);
    // Función para cancelar y redirigir a la lista de hoteles.
  }
}
