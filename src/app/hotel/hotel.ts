export class Hotel {
  id!: number;    // Identificador único del hotel.
  name!: string;  // Nombre del hotel.
  city!: string;  // Ciudad en la que se encuentra el hotel.
  stars!: string; // Número de estrellas del hotel.
}

