import { Component, OnInit } from '@angular/core';
import { HotelFilter } from '../hotel-filter';
import { HotelService } from '../hotel.service';
import { Hotel } from '../hotel';

@Component({
  selector: 'app-hotel',
  templateUrl: 'hotel-list.component.html'
})
export class HotelListComponent implements OnInit {

  filter = new HotelFilter();  // Objeto para almacenar los filtros de búsqueda.
  selectedHotel!: Hotel;      // Variable para almacenar el hotel seleccionado.
  feedback: any = {};         // Variable para almacenar comentarios o retroalimentación.

  get hotelList(): Hotel[] {
    return this.hotelService.hotelList;
  }
  // Propiedad para obtener la lista de hoteles desde el servicio.

  constructor(private hotelService: HotelService) {
  }
  // Constructor que inyecta el servicio de hoteles.

  ngOnInit() {
    this.search();
  }
  // Método que se ejecuta al inicializar el componente y realiza una búsqueda inicial.

  search(): void {
    this.hotelService.load(this.filter);
    // Método para cargar la lista de hoteles utilizando el servicio y el filtro actual.
  }

  select(selected: Hotel): void {
    this.selectedHotel = selected;
    // Método para seleccionar un hotel.
  }

  delete(hotel: Hotel): void {
    if (confirm('¿Estás seguro?')) {
      this.hotelService.delete(hotel).subscribe({
        next: () => {
          this.feedback = {type: 'success', message: 'Se eliminó correctamente!'};
          // Se proporciona retroalimentación en caso de éxito al eliminar.
          setTimeout(() => {
            this.search();
          }, 1000);
          // Después de 1 segundo, se realiza una nueva búsqueda.
        },
        error: err => {
          this.feedback = {type: 'warning', message: 'Error al eliminar.'};
          // Se proporciona retroalimentación en caso de error al eliminar.
        }
      });
    }
  }
  // Método para eliminar un hotel, mostrando un mensaje de confirmación.
}
