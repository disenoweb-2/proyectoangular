import { Component } from '@angular/core';

@Component({
  selector: 'flight-app',  // Selector del componente para su uso en plantillas HTML.
  templateUrl: './app.component.html',  // Ruta al archivo de plantilla HTML asociado con el componente.
  styleUrls: ['./app.component.css']  // Rutas a los archivos de hojas de estilo asociados con el componente.
})
export class AppComponent {
  // Clase del componente raíz de la aplicación.
}
