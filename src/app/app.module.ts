import { FlightModule } from './flight/flight.module'; // Importa el módulo FlightModule.
import { HttpClientModule } from '@angular/common/http'; // Importa el módulo HttpClientModule.

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'; // Importa el módulo BrowserModule.
import { RouterModule } from '@angular/router'; // Importa el módulo RouterModule.

import { AppComponent } from './app.component'; // Importa el componente AppComponent.
import { APP_EXTRA_OPTIONS, APP_ROUTES } from './app.routes'; // Importa opciones de enrutamiento y rutas definidas en app.routes.
import { HomeComponent } from './home/home.component'; // Importa el componente HomeComponent.
import { NavbarComponent } from './navbar/navbar.component'; // Importa el componente NavbarComponent.
import { SidebarComponent } from './sidebar/sidebar.component'; // Importa el componente SidebarComponent.
import { HotelModule } from './hotel/hotel.module'; // Importa el módulo HotelModule.

@NgModule({
  imports: [
    BrowserModule, // Registra el módulo BrowserModule para la aplicación.
    HttpClientModule, // Registra el módulo HttpClientModule para realizar solicitudes HTTP.
    FlightModule, // Registra el módulo FlightModule, que puede contener componentes y servicios relacionados con vuelos.
    RouterModule.forRoot([...APP_ROUTES], {...APP_EXTRA_OPTIONS}), // Configura las rutas de la aplicación.
    HotelModule, // Registra el módulo HotelModule, que puede contener componentes y servicios relacionados con hoteles.
  ],
  declarations: [
    AppComponent, // Registra el componente raíz de la aplicación.
    SidebarComponent, // Registra el componente de la barra lateral.
    NavbarComponent, // Registra el componente de la barra de navegación.
    HomeComponent, // Registra el componente de la página de inicio.
  ],
  providers: [], // Define proveedores de servicios si es necesario.
  bootstrap: [AppComponent] // Especifica el componente raíz para iniciar la aplicación.
})
export class AppModule {
}
