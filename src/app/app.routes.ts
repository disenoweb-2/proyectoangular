import { ExtraOptions, PreloadAllModules, Routes } from '@angular/router'; // Importa clases y tipos relacionados con el enrutamiento.

import { HomeComponent } from './home/home.component'; // Importa el componente HomeComponent.

// Define las rutas de la aplicación.
export const APP_ROUTES: Routes = [
  {
    path: '', // Ruta raíz (cuando la URL está vacía).
    redirectTo: 'home', // Redirige a la ruta 'home'.
    pathMatch: 'full' // Se requiere una coincidencia completa para la redirección.
  },
  {
    path: 'home', // Ruta 'home'.
    component: HomeComponent // Asocia la ruta con el componente HomeComponent.
  },
  {
    path: '**', // Ruta comodín que coincide con cualquier ruta no definida previamente.
    redirectTo: 'home' // Redirige a la ruta 'home'.
  }
];

// Define opciones adicionales para el enrutamiento.
export const APP_EXTRA_OPTIONS: ExtraOptions = {
  preloadingStrategy: PreloadAllModules // Estrategia de precarga para cargar módulos perezosos (lazy loading) al inicio.
};

