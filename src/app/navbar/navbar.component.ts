import { Component } from '@angular/core';

@Component({
  selector: 'navbar-cmp', // Selector del componente para su uso en plantillas HTML.
  templateUrl: 'navbar.component.html' // Ruta al archivo de plantilla HTML asociado con el componente.
})
export class NavbarComponent {

  private sidebarVisible: boolean = false; // Variable privada para controlar la visibilidad de la barra lateral.

  constructor() {
    // Constructor del componente.
  }

  sidebarToggle() {
    // Método para alternar la visibilidad de la barra lateral.
    const body = document.getElementsByTagName('body')[0]; // Obtener el elemento <body> del documento.

    if (!this.sidebarVisible) {
      // Si la barra lateral no es visible, se muestra.
      body.classList.add('nav-open'); // Agrega una clase CSS al cuerpo del documento para mostrar la barra lateral.
      this.sidebarVisible = true; // Actualiza la variable para indicar que la barra lateral es visible.
      console.log('making sidebar visible...'); // Registro de un mensaje en la consola.
    } else {
      // Si la barra lateral es visible, se oculta.
      this.sidebarVisible = false; // Actualiza la variable para indicar que la barra lateral no es visible.
      body.classList.remove('nav-open'); // Remueve la clase CSS del cuerpo del documento para ocultar la barra lateral.
    }
  }
}
