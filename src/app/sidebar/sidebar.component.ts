import { Component } from '@angular/core';

@Component({
  selector: 'sidebar-cmp',  // Selector del componente para su uso en plantillas HTML.
  templateUrl: 'sidebar.component.html',  // Ruta al archivo de plantilla HTML asociado con el componente.
})

export class SidebarComponent {
}
